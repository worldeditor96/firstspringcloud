package com.example.eurecaserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class EurecaClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(EurecaClientApplication.class, args);
        System.out.println("Ready");
    }

}
